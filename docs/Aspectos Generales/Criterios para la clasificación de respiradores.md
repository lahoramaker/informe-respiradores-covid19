# Criterio de clasificación para los respiradores

Dada la aparición de numerosos modelos y variantes de respiradores, se hace necesario definir una serie de criterios para su clasificación. Al igual que ocurre con otras tecnologías, no todos los dispositivos sirven para todos los escanarios ni todos serán igual de operativos en la crisis del Covid-19.

## Ejes para la clasificación
### Tiempo estimado de uso
En función del tiempo puedan ofrecer respiración mecánica a un paciente, podemos definir las siguientes cuatro categorías:

 * **Respiradores para traslados** que pueden permitir sobrevivir a los pacientes en el trayecto en ambulancia desde el lugar de un accidente, hasta un hospital mejor equipado. Estos respiradores pueden trabajar de forma segura en el rango de **1-3 horas**.

 * **Respiradores de emergencia/campo** que están más pensados para un uso temporal, en el que no se va a trasladar al paciente. Serían respiradores para hospitales de campaña, donde el paciente podría permanecer durante **2-3 días** antes de ser trasladado a otras instalaciones mejor equipada.

 * **Respiradores de duración media** serán aquellos que permitan la ventilación mecánica de los pacientes durante un periodo de entre **1-3** semanas. En estos respiradores, se utiliza el respirador para cubrir una afección pulmonar grave del paciente que tiene una posible recuperación prolongada en el tiempo.

 * **Respiradores de larga duración** serían aquellos respiradores que están pensados para el soporte vital de pacientes a largo plazo, afectados por enfermedades crónicas. El ejemplo más claro de este tipo de respirador son los pulmones de hierro (iron lung) que se popularizaron en siglo XX para tratar a los pacientes afectados por la poliomelitis.

### Necesidades temporales en el caso de pacientes CoVID-19
Según la nota de la Sociedad Europea de Anestesiología sobre los [tratamientos en Italia y Lombardía del CoVID-19 en cuidados intensivos](https://www.esahq.org/esa-news/dynamics-of-icu-patients-and-deaths-in-italy-and-lombardy-due-to-covid-19-analysis-updated-to-30-march-day-38-evening/), los pacientes críticos afectados por CoVID-19 deben permanecer en un hospital con ventilación mecánica durante un periodo aproximado de 15 días.

## Tipo de afección que puede tratar en función de su complejidad
Los respiradores tienen toda una serie de características técnicas que permiten tratar distintos tipos de insuficiencia respiratoria.

En un extremo del espectro nos encontraríamos a los **respiradores manuales**, que deben accionarse continuamente por médicos especialistas. Su uso estaría limitado claramente a situaciones de emergencia, ya que pueden generar importantes daños si se emplean erroneamente o de forma extendida en el tiempo.

*Nota: Ventilacion invasiva frente a no invasiva*

Existe [literatura médica](https://www.ncbi.nlm.nih.gov/books/NBK441924/) que nos indica que a largo plazo, la respiracion a largo plazo de pacientes utilizando BVM, puede generar problemas por presión excesiva en los pulmones (barotrauma), la entrada de aire en el estómago y la salida de ácidos de esta zona que dañen los pulmones.

En el otro extremo, nos encontraremos **respiradores muy sofisticados** que nos permitirán controlar toda una serie de parámetros para ajustarnos de forma adecuada a la evolución de los pacientes.

En los primeros días del estado de alarma, el doctor Pancho Cañizo, del Hospital Gregorio Marañon, contacto con diversos especialistas que estaban trabajando con pacientes CoVID-19 para generar una serie de parámetros base. Estos parámetros son los mínimos que permitirían tratar a este tipo de pacientes afectados por CoVID-19 que requieren respirador.

### Uso de máscaras frente a intubación
Revisando los parámetros podemos observar que se pueden requerir flujos y presiones muy altas, para hacer que llegue el oxigenos suficiente a los pulmones dañados. En el caso de pacientes CoVID-19 se da una sintomatología conocida como pulmón duro. En estos casos, el pulmón es más dificil de mantener ya que hace falta un control preciso respecto al "flujo, pendiente de flujo, presión máximo, presión mantenida PEEP, etc." algo que según compartió el [Dr. Armengol en el foro](https://foro.coronavirusmakers.org/index.php?p=/discussion/66/abogado-del-diablo-con-canas), sólo puede conseguirse a través de un control computerizado.

Para tratar estos casos se requiere mayor presión inspiratoria. Este hecho, hace que las mascarillas no sean recomendadas, ya que con estas elevadas presiones, parte del aire se perderá por las junturas de entre la máscara y la cara. Otro efecto colateral del uso de mascarillas es que los virus podrían aerosolizarse, provocando una mayor concentración del virus en ambiente.

*Nota: Para evitar liberar el virus en formato aerosol, En el caso de pacientes CoVID19, es necesario siempre filtrar la salida de la expiración.*

### Funcionalidades mínimas requeridas para tratar un paciente grave CoVID-19

Una de las funcionalidades más requeridas por los médicos consultados es la capacidad del controlar el PEEP. PEEP significa Positive end-expiratory pressure o presión positiva al final de la expiración. El PEEP es una medida de la presión restante en los pulmones del paciente una vez acabe su ciclo de expiración.
El control del PEEP es clave para que el dispositivo resulte útil y no dañe de forma masiva al paciente. Una presión demasiado baja puede producir que los alveolos colapsen, mientras que una presión muy elevada puede dañar los pulmones de forma crítica por barotrauma.
Para más información a este respecto puede consultarse la publicación: Tomicic, V., Fuentealba, A., Martínez, E., Graf, J., & Batista Borges, J. (2010). [Fundamentos de la ventilación mecánica en el síndrome de distrés respiratorio agudo](http://scielo.isciii.es/pdf/medinte/v34n6/revision.pdf). Medicina intensiva, 34(6), 418-427.

# Clasificación inicial de los respiradores
En base a los criterios anteriores, vamos a definir la siguiente clasificación:

 * **Respiradores orientados a pacientes Covid-19**: Son aquellos que están pensandos para atender a pacientes con distress pulmonar durante un periodo de dos semanas. Ofrecen control de los distintos parámetros de forma precisa, siguiendo las especificaciones básicas, incluyendo control del PEEP. Deben poder funcionar con intubación.

 * **Respiradores de emergencia (temporales)**: permiten tratar de forma puntual a una persona durante un periodo más reducido (1-2 días como máximo). El control de los parámetros respiratorios es limitado. Pueden funcionar sin intubación con medios menos invasivos.

 * **Respiradores auxiliares**: Permiten suministrar al paciente una mayor cantidad de oxígeno XXX, durante fases menos críticas de las enfermedad. El control de los distintos parámetros es mínimo.


# Otros parámetros relevantes
Además de los parámetros indicados anteriormente, este informe recogerá la siguiente información de cada uno de los respiradores:

## Respiradores según tecnología
Dentro de los requerimientos indicados en el anterior apartado, existen numerosas formas de conseguir este tipo de respiración mecánica. Describiremos cuál es el tipo de tecnología empleada por el respirador para funcionar.

## Modos de funcionamiento
A medida que ha ido avanzando la tecnología se ha ido ofreciendo una mayor complejidad en el mundo de los respiradores.

Los primeros respiradores funcionaban en modeo **presión control**. En este modo se puede regular de forma precisa la presión de aire suministrada.
Los respiradores más modernos permiten funcionar en modo **volumen control** que garantiza un volumen constante de aire en los pulmones del paciente.
Algunos ventiladores más complejos permiten funcionar en modo **assisted control** (control asistido). Este modo permite la [configuración de cuatro parámetros](https://www.ncbi.nlm.nih.gov/books/NBK441856/) de forma rápida: Volumen tidal, número de respiraciones por minuto, FiO2 (fracción de oxígeno inspirado), PEEP.

## Respiradores según tipo de licencia
 * Cerrados: serán aquellos de los que no se ofrezca por parte del fabricante información suficiente para permitir su replicación de forma independiente.
 * Patentados: serán aquellos en los que el fabricante haya compartido su invención a través de una patente, lo que le otorga un derecho de explotación exclusivo de la misma.
 * Abiertos/Libres: serán aquellos en los que el fabricante o diseñador hayan compartido su diseño con una licencia libre o abierta, que permita a cualquier persona su replicación.
 * Abiertos temporalmente: serán aquellos respiradores cuyo diseño se ha compartido para permitir su replicación, pero cuyo uso está limitado en el tiempo a la duración de la pandemia del covid-19.
