# Datos comunes para todos los respiradores
Para cada uno de los proyectos analizados vamos a recoger la información que aparece en la siguiente plantilla:

**Nombre**: Nombre del proyecto

**Descripción breve**: Breve descripción del proyecto	

**Pais**: Pais(es) donde se origina el proyecto

**Región**: Zona del pais donde se origina el proyecto	

**Equipo**: Listado de personas o instituciones creadoras del proyecto

**Fecha de lanzamiento**: Indica la primera fecha en la que se lanzó el proyecto de forma pública (irá referenciado a la fuente principal).

**Origen Maker**: Indica si este proyecto se ha originado en una comunidad maker (reconociéndose como tal los propios integrantes del grupo).
  * 0 El proyecto no se ha originado en una comunidad maker
  * 1 El proyecto se ha originado en una comunidad maker
  * -1 Se desconoce (o no está claro) si el proyecto se ha originado en una comunidad maker

**Nivel máximo de evolución alcanzado anunciado**: Indica el máximo nivel de evolución alcanzado por el proyecto, comunicado de forma pública y demostrable documentalmente. Puede tomar uno de los siguientes valores:

  * 0 Idea inicial
  * 1 Primer prototipo
  * 2 Prototipo funcional
  * 3 Pruebas con simulador
  * 4 Pruebas con animales sanos
  * 5 Pruebas con animales patológicos
  * 6 Certificación con laboratorio externo
  * 7 Pruebas clínicas con pacientes humanos
  * 8 Validación temporal durante la crisis
  * 9 Homologación del respirador
  * -1 No disponible

**Nivel máximo de evolución alcanzado no oficial**: Es identico al anterior, aunque no se puede corroborar la información públicamente. Por lo general, será el nivel comunicado verbalmente o a través de otros medios, pero no se ha publicitado todavía.

**Descripción pruebas máximo nivel**: Indica de forma expandida las pruebas alcanzadas de máximo nivel (p.e Prueba exitosa con cerdo patológico de 11 horas). Se considera no oficial a no ser que se acompañe de fuente correspondiente.

**Afección tratable según complejidad**: Indica el nivel máximo de complejidad del dispositivo, de cada a afrontar distintos tipos de afecciones:
  
  * 0 Respirador manual
  * 1 Respirador con controles simples (control mecánico/no sensorizado)
  * 2 Respirador con controles avanzados (sensorizado)
  * 3 Respirador con controles muy avanzados (sensorizado y multimodo)
  * -1 Se desconoce la complejidad del sistema de control

**Tecnología empleada**: Describe el tipo de tecnología principal empleada para suministrar la mezcla de gases.
  
  * 0 Neumático
  * 1 Electroválvulas
  * 2 Mecánico
  * 3 Otros
  * -1 Desconocida
 
**Modos de funcionamiento**: Indica los distintos modos con los que puede funcionar el aparato:
  
  * 0 Pressure control
  * 1 Volume control
  * 2 Trigger/Assist Control
  * 3 Pressure control + Volume Control + Trigger Control
  * 4 Volume control + Assist Control
  * -1 No disponible
 
**¿Permite intubación?**: Indica si el dispositivo puede utilizarse con pacientes intubados
  
  * 0 No puede utilizarse con pacientes intubados
  * 1 Puede utilizarse con pacientes intubados
  * -1 No se sabe si puede utilizarse o no con pacientes intubados
 
**Tiempo de aplicación**: Indica el tiempo máximo que puede utilizarse el respirador con un paciente, en base al diseño y pruebas realizadas. Se tabula con los siguientes valores:
  
  * 0 1-2 Horas
  * 1 1 Día
  * 2 2-3 Días
  * 3 1-3 Semanas
  * 4 Varios meses
  * -1 No se sabe el tiempo que puede utilizarse con un paciente.

**Sitio web**: Indica la dirección del sitio web del proyecto

**Licencia del proyecto**: Indica la licencia empleada para el proyecto de forma descriptiva.

**Ficheros disponibles**: Indica si los fuentes del proyecto están disponibles para permitir su replicabilidad.

  * 0 Los ficheros no están disponibles en el sitio web del proyecto
  * 1 Algunos ficheros están disponibles en el sitio web del proyecto, pero son insuficientes para reproducir el respirador
  * 2 Los ficheros están disponibles en el sitio web del proyecto
  * 3 Las instrucciones están disponibles en el sitio web del proyecto
  * 4 Los ficheros y las instrucciones de montaje están disponibles en el sitio web del proyecto
 * -1 Se desconoce

**Proyecto replicado con éxito**: Indica si este proyecto se ha replicado en algún lugar adicional y si las personas que replicaron han confirmado su funcionamiento
  * 0 El proyecto no se ha replicado más allá del propio equipo promotor del proyecto
  * 1 El proyecto se ha replicado en una ocasión pero no se ha reportado su funcionamiento
  * 2 El proyecto se ha replicado en una ocasión y se ha reportado su funcionamiento
  * 3 El proyecto se ha replicado en múltiples ocasiones pero no se ha reportado su funcionamiento
  * 4 El proyecto se ha replicado en múltiples ocasiones y se ha reportado su funcionamiento
  * -1 Se desconoce si el proyecto se ha replicado por un tercero

**Fuente principal**: Indica la publicación o nota de prensa desde la que se han recogido los datos de este listado. En ausencia de publicación pública, puede referenciarse también el sitio web del proyecto.

**Dirección de contacto**: Dirección de email del contacto principal del proyecto

**Twitter**: Dirección de Twitter del proyecto

**Facebook**: Dirección de Facebook del proyecto

**Instagram**: Dirección de Instagram del proyecto

**Parámetros clínicos conseguidos**: Indica si se dispone de datos clínicos del proyecto, tales como el PEEP, I/E, Flujo máximo, Volumen tidal
  
  * 0 Existen datos disponibles
  * -1 No existen datos disponibles
