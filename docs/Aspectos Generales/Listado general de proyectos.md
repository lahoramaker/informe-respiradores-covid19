# Listado general de proyectos
En este listado encontraremos referencias a todos los proyectos considerados, detallando el grado de desarrollo de sus fichas.

## Nuevos respiradores
* Leitat-1
* Reesistencia
* Oxygen
* Apollo Project
* Sirio Pump
* Coronavirus Maker CPAP
* Coronavirus Maker VEDA
* Respirador Pacho Cañizo
* ITAINNOVA-Ebers Medical
* Malaga Respira-3
* AirGyver
* UCLM
* Elche Salesianos
* Elche Maxiwatt
* Respirapadre
* Bionix
* Respirapadre
* Grupo de trabajo STM32
* The Open Ventilator

* MIT e-Vent Project - USA (Mass.)
* Open Source Ventilator - USA / Irlanda
* Open Source Ventilator Project - USA (Florida)
* VentilAid - Polonia
* VUB Rapid-Manufacturing Ventilator - Belgica
* Breath4life - Belgica
* ReesistenciaTMx - México
* Ventilador Monterrey - México
* Beatmungsgerät - Alemania
* jcl5m1 PAPR Ventilator - USA (California)
* covid19ventilador - USA (Minnesota)
* Pandemic Ventilator


## Otros respiradores
* Respirador Dr. Lucas Picazo / Polisur - España (Cádiz)
* RICE OEDK Ventilator - USA (Texas)
* Prototipo de respirador artificial de bajo costo - Ecuador
* MIT Low-Cost Ventilator - USA (Mass.)
* Umbulizer - USA (Mass.)
* Darwood Low-Cost Portable Ventilator - UK

# Otros dispositivos de ayuda a la respiración
* Isinnova - Italia
* Mascara Decathlon (oficial)
* Máscara Decathlon (Makers)
* Máscara Cressi (Makers)
* Máscara Mediamarkt (Makers)
* Oxygen Concentrator - UK
