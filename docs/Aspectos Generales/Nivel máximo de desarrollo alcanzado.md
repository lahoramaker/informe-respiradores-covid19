# Nivel máximo de desarrollo alcanzado
En la actualidad contamos con una elevada cantidad de respiradores, tanto de código abierto como propietarias. Es importante clarificar en qué etapa se encuentran cada uno de los modelos, para conocer a ciencia cierta, cuáles están podrían fabricarse con garantías.

## TRL - TBD 


## Pasos mínimos para la validación de un respirador
En la siguiente escala podemos encontrar cuales son las distintas fases por las que tiene que pasar un respirador, desde el punto de vista legal/funcional, desde su concepción inicial hasta su despliegue final a escala.

```mermaid
graph TD
  A(0. Idea Inicial) --> B(1. Primer prototipo)
  B --> C(2. Prototipo funcional)
  C --> D(3. Pruebas con simulador)
  D --> E(4. Pruebas con animales sanos)
  E --> F(5. Pruebas con animales patológicos - opcional)
  F --> G(6. Certificación con laboratorio externo - opcional)
  G --> H(7. Entregada la documentación a la AEMPS)
  H --> I(8. Aprobación temporal durante la crisis para pruebas médicas)
  I --> J(9. Investigación clínica con pacientes humanos)
  J --> K(10. Homologación del respirador)
```

El orden de estos pasos está extraido de la [nota publicada](https://www.aemps.gob.es/informa/notasInformativas/productosSanitarios/2020/NI-PS_11-2020-respiradores-pruebas-investigaciones.pdf?x33378) por la Agencia Española de Medicamentos y Productos Sanitarios el día 1 de Abril de 2020. De cara a la validación del respirador, se exige que este pase unas pruebas de compatibilidad electromagnética para asegurar que no interfiere con otros dispositivos de la UCI y que no será afectado por otras máquinas cercanas. Esta prueba estaría incluida dentro del paso 6. (Certificación con laboratorio externo) pero la propia AEMPS se ha ofrecido a realizarla a aquellos equipos que no tengan acceso a uno de estos laboratorios, debiendo remitirse el prototipo a sus instalaciones:

La nota de la AEMPS hace referencia en sus requisitos a las siguientes normas existentes para los respiradores:

* UNE EN 60601-2-12: Equipos electromédicos. Parte 2-12, Requisitos particulares para la seguridad de los ventiladores pulmonares : ventiladores de cuidados críticos : (IEC 60601-2-12:2001), Partes 2-12
* UNE EN 80601-1: Equipos electromédicos. Parte 2-12: Requisitos particulares para la seguridad básica y funcionamiento esencial de los respiradores para cuidados intensivos.(ISO 80601-2-12:2011)
* UNE EN 794-3: Respiradores pulmonares. Parte 3: Requisitos particulares de los respiradores para emergencias y transporte.

Las dos primeras normas hacen referencia a los respiradores más complejos, mientras que la tercera se aplica a respiradores de emergencia y transporte. En la [reunión mantenida el día 28 de Marzo de 2020 con la AEMPS](https://foro.coronavirusmakers.org/index.php?p=/discussion/724/20200330-resumen-de-conferencia-respiradores-con-agencia-del-medicamento), se indicó que algunos de los dispositivos más simples, que mecanizaban el movimiento de un AMBU (o otro tipo de bolsa), no tenían una norma asociada como tal, por lo que tendría que examinarse la documentación Ad hoc para estos casos.

Otras administraciones como el MHRA ha facilitado unas normas simplificadas para la validación de respiradores de manufactura rápida (RMVS según sus siglas en inglés). En este proceso, las pruebas a realizar están también simplicadas y omiten algunos pasos como las pruebas con animales. Los requisitos de estas pruebas están disponibles dentro del [apendice 2](https://assets.publishing.service.gov.uk/government/uploads/system/uploads/attachment_data/file/876167/RMVS001_v3.1.pdf) de la publicación.

Por lo general, el proceso de homologación completo de un respirador es mucho más complejo. En estos procesos se evaluan las instalaciones, la operativa, se incorporan auditorias periódicas y aleatorias para controlar la calidad, etc.
