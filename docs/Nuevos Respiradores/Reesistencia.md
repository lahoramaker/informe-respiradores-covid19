# Nombre: Reesistencia
## Descripción breve

## Detalles técnicos
Tipo de respirador: 
Tecnología empleada: Neumática / 

Tipo de licencia:

## Detalles clínicos
Modos de funcionamiento: (eliminar los que no correspondan) Pressure control / Volume Control / Assisted Control
Parámetros operativos: TBD

## Detalles normativos
Nivel máximo de desarrollo alcanzado:
Homologado:

## Descripción general
Este proyecto se lanzó a partir del encuentro de dos personas en el canal de Telegram de Coronavirus Makers. Este grupo inicial lo forman Marcos Castillo, ingeniero y Ramses Marrero, médico. A partir de la experiencia de Ramses, se comienza el desarrollo entorno al sistema Jackson Rees, que permite un uso más prolongado.

El prototipo está funcional y se ha probado, hasta donde sé, con simulador. Están pendientes del resto de pruebas, pero ya muy conectados con el ecosistema de salud asturiano.

El proyecto está en estos momentos en pruebas en uno de los hospitales de Asturias y está pendiente de validación.

https://twitter.com/ReesistenciaT
