# Informe Respiradores Covid-19
Este informe recoge los datos relativos a los respiradores desarrollados con motivo de la pandemia del SARS-Cov2, iniciada a finales de 2019.

## Objetivos del documento
Este documento aspira a recoger información suficiente sobre todos los proyectos de respiradores que han surgido para combatir la crisis del CoVID-19. Este informe está especialmente orientado a aquellas iniciativas que han surgido, tanto de caracter maker como no maker, para ofrecerles información de contexto sobre los pasos a realizar para que su prototipo llegue a buen puerto. Así mismo, el informe busca poner el valor todo el trabajo desarrollado por estos equipos para conseguir generar soluciones que, en esta situación de emergencia, pueden salvar vidas.

Por otra parte, queremos ofrecer información de contexto sobre los requisitos para un respirador útil en estos momentos, para ayudar a ubicar los usos potenciales de cada dispositivo. Existe mucha confusión respecto a los distintos tipos de dispositivos y queremos arrojar algunas premisas clave, para detectar necesidades resueltas u oportunidades por cubrir en el ámbito clínico.

Por último, nos gustaría aportar claridad respecto al grado de evolución de los proyectos. En los últimos días se están publicando numerosas noticias, en ocasiones contradictorias respecto al estado y avances de los distintos respiradores. Nuestro objetivo es ofrecer información contrastada sobre el desarrollo y formas utilización de este tipo de dispositivos, referenciando siempre documentación fidedigna.

## Control de versiones
### V0.1 - Inicial
En esta versión inicial del informe vamos a ofrecer datos de aquellos respiradores que han alcanzado un nivel de desarrollo más avanzado en los últimos días ([mayor a 4](/Aspectos Generales/Nivel máximo de desarrollo alcanzado.md)). Esta información se recogerá en un resumen ejecutivo, que se irá ampliando en el tiempo.

Además del propio texto, el informe consta también de una base de datos de los proyectos, donde se está recopilando la información para su análisis estadístico. Puede consultarse el formato de este fichero en el documento de [datos comunes para todos los respiradores](Aspectos Generales/Datos comunes a recoger de los distintos respiradores.md).

## Organización del documento
Este documento está divido en distintas secciones para facilitar su lectura:

* Resumen ejecutivo
* Aspectos generales sobre respiradores y el Covid-19
* Descripción general de los nuevos proyectos de respirador
* Otros respiradores DIY/low-cost
* Otros dispositivos de ayuda a la respiración

## Disclaimer y Política de actualizaciones
En primer lugar, me gustaría aclarar que no soy médico, ni experto en respiradores o certificaciones de dispositivos médicos. Por este motivo, he tratado de referenciar cada aspecto crítico del texto con un su correspondiente referencia técnica o publicación científica.

Mi intención es que esta documentación esté en constante evolución durante esta crisis, recogiendo actualizaciones periódicas. Si quieres ampliar o corregir la información que aparece en este documento, puedes hacerlo enviando tus contribuciones en forma de pull requests, o enviándolas por correo a respiradores arroba lahoramaker punto com. Sólo se considerarán aquellas actualizaciones debidamente referenciadas (En el caso de los respiradores, buscamos siempre referencias en medios y/o documentación técnica).

## Licencia
El contenido de este informe así cómo el de sus derivados (pdfs, etc) está licenciados bajo una licencia Creative Commons BY-SA 4.0 Internacional. Para más información sobre sus derechos para utilizar estos contenidos: https://creativecommons.org/licenses/by-sa/4.0/

El autor de la versión inicial de esta obra es César García Sáez para La Hora Maker. Se añadirá la información adicional de colaborares adicionales, en base a sus aportaciones.

## Agradecimientos
Quisiera agradecer a Pedro Amador Rodriguez y el resto de integrantes del grupo TIC para Bien, por su inspiración para compartir la información en este formato dinámico y colaborativo.

Así mismo, quisiera agradecer a Jorge Barrero, Esther Borao y Fernando Quero, por hacerme llegar información respecto a algunos de estos respiradores.

Por último agradecer la labor de Robert L. Read de PublicInv, para recopilar una lista de proyectos de respiradores de código abierto y compartirla a su vez abiertamente.
